FROM adoptopenjdk/openjdk11-openj9:jre-11.0.4_11_openj9-0.15.1
VOLUME /tmp
COPY build/libs/*.jar app.jar
ENTRYPOINT ["java","-jar","/app.jar"]