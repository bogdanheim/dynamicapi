package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class UltimateParent {

    private Exporter exporter;
    private Importer importer;
    private String industry;
    private String legalForm;
    private String lei;
    private String name;

    public static UltimateParent getDefault() {
        UltimateParent u = new UltimateParent();
        u.setExporter(null);
        u.setImporter(null);
        u.setIndustry("Industry");
        u.setLegalForm("SA");
        u.setLei("6Cai");
        u.setName("UltimateParent");
        return u;
    }
}
