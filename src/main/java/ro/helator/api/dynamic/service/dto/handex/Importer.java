package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Importer {

    private String applicationDataId;
    private List<CashflowStatement> cashflowStatement;
    private CompanyInformation companyInformation;
    private DirectParent directParent;
    private String importerId;
    private UltimateParent ultimateParent;

    public static Importer getDefault() {
        return getDefault("ID1");
    }

    public static Importer getDefault(String appId) {
        Importer i = new Importer();
        i.setApplicationDataId(appId);
        i.setCashflowStatement(CashflowStatement.getDefaultList());
        i.setCompanyInformation(null);
        i.setDirectParent(DirectParent.getDefault());
        i.setImporterId("Importer1");
        i.setUltimateParent(UltimateParent.getDefault());
        return i;
    }
}
