package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Cashflow {

    private Double amount;
    private Integer offsetDisbursementDate;

    public static List<Cashflow> getDefaultList() {
        List<Cashflow> list = new ArrayList<>();
        for (int i=0; i<2; i++){
            list.add(Cashflow.getDefault(i));
        }
        return list;
    }

    private static Cashflow getDefault() {
        return getDefault(0);
    }

    private static Cashflow getDefault(int i) {
        Cashflow c = new Cashflow();
        c.setAmount(10D * (i+1));
        c.setOffsetDisbursementDate(3);
        return c;
    }
}
