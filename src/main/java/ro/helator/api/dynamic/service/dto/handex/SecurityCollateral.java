package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class SecurityCollateral {

    private String depositer;
    private Date guaranteeValidUntil;
    private String isin;
    private Double notional;

    public static SecurityCollateral getDefault() {
        SecurityCollateral s = new SecurityCollateral();
        s.setDepositer("disp");
        s.setGuaranteeValidUntil(new Date());
        s.setIsin("isin");
        s.setNotional(3D);
        return s;
    }
}
