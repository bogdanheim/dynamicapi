package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Interest {

    private Integer couponPeriodMonths;
    private Double discountRate;
    private Double fixedCouponPerAnnum;
    private String variableCouponReferenceRate;
    private Double variableCouponSpread;

    public static Interest getDefault() {
        Interest i = new Interest();
        i.setCouponPeriodMonths(2);
        i.setDiscountRate(2D);
        i.setFixedCouponPerAnnum(12D);
        i.setVariableCouponReferenceRate("rate");
        i.setVariableCouponSpread(3D);
        return i;
    }
}
