package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Exporter {

    private String applicationDataId;
    private List<BalanceSheet> balanceSheet;
    private List<CashflowStatement> cashflowStatement;
    private CompanyInformation companyInformation;
    private DirectParent directParent;
    private EulerHermesData eulerHermesData;
    private String exporterId;
    private KycData kycData;
    private List<ProfitAndLoss> profitAndLoss;
    private UltimateParent ultimateParent;

    public static Exporter getDefault() {
        return getDefault("ID1");
    }

    public static Exporter getDefault(String appId) {
        Exporter e = new Exporter();
        e.setApplicationDataId(appId);
        e.setBalanceSheet(BalanceSheet.getDefaultList());
        e.setCashflowStatement(CashflowStatement.getDefaultList());
        e.setCompanyInformation(CompanyInformation.getDefault());
        e.setDirectParent(DirectParent.getDefault());
        e.setEulerHermesData(EulerHermesData.getDefault());
        e.setExporterId("exp1");
        e.setKycData(KycData.getDefault());
        e.setProfitAndLoss(ProfitAndLoss.getDefaultList());
        e.setUltimateParent(UltimateParent.getDefault());
        return e;
    }
}
