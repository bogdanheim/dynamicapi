package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DocumentModel {

    private String applicationId;
    private byte[] data;
    private String fileName;
    private String fileType;

    public static DocumentModel getDefault() {
        return getDefault("ID1");
    }

    public static DocumentModel getDefault(String applicationId) {
        DocumentModel d = new DocumentModel();
        d.setApplicationId(applicationId);
        d.setData("Test Document Content".getBytes());
        d.setFileName(applicationId + "-doc");
        d.setFileType("TXT");
        return d;
    }
}
