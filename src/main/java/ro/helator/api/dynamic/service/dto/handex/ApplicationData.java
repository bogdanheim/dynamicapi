package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ApplicationData {

    private String applicationId;
    private Contract contract;
    private String email;
    private EulerHermesCoverage eulerHermesCoverage;
    private Exporter exporter;
    private ForfaitingAgreement forfaitingAgreement;
    private Guarantee guarantee;
    private Importer importer;
    private Interest interest;
    private Repayment repayment;
    private SecurityCollateral securityCollateral;


    public static ApplicationData getDefault(String applicationId) {
        ApplicationData applicationData = new ApplicationData();
        applicationData.setApplicationId(applicationId);
        applicationData.setContract(Contract.getDefault(applicationId));
        applicationData.setEmail("test@mail.com");
        applicationData.setEulerHermesCoverage(EulerHermesCoverage.getDefault());
        applicationData.setExporter(Exporter.getDefault(applicationId));
        applicationData.setForfaitingAgreement(ForfaitingAgreement.getDefault());
        applicationData.setGuarantee(Guarantee.getDefault());
        applicationData.setImporter(Importer.getDefault(applicationId));
        applicationData.setInterest(Interest.getDefault());
        applicationData.setRepayment(Repayment.getDefault());
        applicationData.setSecurityCollateral(SecurityCollateral.getDefault());
        return applicationData;
    }
}
