package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Payment {

    private String accountNumber;
    private List<Arrear> arrears;
    private Date bookingDate;
    private String currencyIso;
    private Double fee;
    private Double installment;
    private Double interest;
    private Double repayment;

    public static List<Payment> getDefaultList() {
        List<Payment> list = new ArrayList<>();
        for(int i=0; i<3; i++){
            list.add(getDefault(i));
        }
        return list;
    }

    public static Payment getDefault(int i) {
        Payment p = new Payment();
        p.setAccountNumber("ROXB23451212" + i);
        p.setArrears(Arrear.getDefaultList());
        p.setBookingDate(new Date());
        p.setCurrencyIso("EUR");
        p.setFee(5D * (i+1));
        p.setInstallment(5D * (i+1));
        p.setInterest(5D * (i+1));
        p.setRepayment(5D * (i+1));
        return p;
    }
}
