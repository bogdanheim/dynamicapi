package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EulerHermesData {

    private String claimsManagement;
    private String dnnr;
    private String exporterId;
    private String marketKnowHow;
    private String prevExperienceExplain;
    private Boolean previousExperience;

    public static EulerHermesData getDefault() {
        EulerHermesData e = new EulerHermesData();
        e.setClaimsManagement("Claim");
        e.setDnnr("dnnr1");
        e.setExporterId("Exp1");
        e.setMarketKnowHow("No");
        e.setPrevExperienceExplain("No");
        e.setPreviousExperience(Boolean.FALSE);
        return e;
    }
}
