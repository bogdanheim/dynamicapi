package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DirectParent {

    private String exporterId;
    private String importerId;
    private String industry;
    private String legalForm;
    private String lei;
    private String name;

    public static DirectParent getDefault() {
        DirectParent d = new DirectParent();
        d.setExporterId("Exp1");
        d.setImporterId("Imp1");
        d.setIndustry("industry");
        d.setLegalForm("SA");
        d.setLei("3Lei");
        d.setName("DirectParent");
        return d;
    }
}
