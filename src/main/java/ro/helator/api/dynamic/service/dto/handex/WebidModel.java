package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WebidModel {

    private String applicationId;
    private byte[] data;
    private String fileName;
    private String fileType;

    public static WebidModel getDefault() {
        return getDefault("ID1");
    }

    public static WebidModel getDefault(String applicationId) {
        WebidModel w = new WebidModel();
        w.setApplicationId(applicationId);
        w.setData("Sote test data".getBytes());
        w.setFileName(applicationId + "-FileName");
        w.setFileType("TXT");
        return w;
    }
}
