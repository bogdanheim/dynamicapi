package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Event {

    private Date milestoneDate;
    private String milestoneDescription;
    private String milestoneId;
    private String milestoneType;

    public static List<Event> getDefaultList() {
        List<Event> list = new ArrayList<>();
        for(int i=0; i < 3; i++){
            Event event = getDefault();
            event.setMilestoneDescription("Desc " + i);
            event.setMilestoneId("MS-" + i);
            list.add(event);
        }
        return list;
    }

    public static Event getDefault() {
        Event event = new Event();
        event.setMilestoneDate(new Date());
        event.setMilestoneType("milestoneType");
        return event;
    }
}
