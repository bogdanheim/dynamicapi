package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class AccountModel {

    private String applicationId;
    private String iban;

    public static AccountModel getDefault() {
        return getDefault("ACC1");
    }

    public static AccountModel getDefault(String applicationId) {
        AccountModel a = new AccountModel();
        a.setApplicationId(applicationId);
        a.setIban("ROXA24562142453123");
        return a;
    }
}
