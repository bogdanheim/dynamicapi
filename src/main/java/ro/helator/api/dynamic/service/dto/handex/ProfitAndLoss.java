package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ProfitAndLoss {

    private Date dateOfFinancialStatement;
    private Double depreciationAmortize;
    private Double ebit;
    private Double ebitda;
    private Double ebt;
    private Double extraordinaryExpenditure;
    private Double extraordinaryIncome;
    private Double financialExpenditure;
    private Double financialIncome;
    private Double netIncome;
    private Double revenue;
    private Double taxes;

    public static List<ProfitAndLoss> getDefaultList() {
        List<ProfitAndLoss> list = new ArrayList<>();
        for (int i =0 ; i <2 ; i++) {
            ProfitAndLoss p = getDefault(i);
            list.add(p);
        }
        return list;
    }

    private static ProfitAndLoss getDefault() {
        return getDefault(0);
    }

    private static ProfitAndLoss getDefault(int i) {
        ProfitAndLoss p = new ProfitAndLoss();
        p.setDateOfFinancialStatement(new Date());
        p.setDepreciationAmortize(2D * (i+1));
        p.setEbit(2D * (i+1));
        p.setEbitda(2D * (i+1));
        p.setEbt(2D * (i+1));
        p.setExtraordinaryExpenditure(2D * (i+1));
        p.setExtraordinaryIncome(2D * (i+1));
        p.setFinancialExpenditure(2D * (i+1));
        p.setFinancialIncome(2D * (i+1));
        p.setNetIncome(2D * (i+1));
        p.setRevenue(2D * (i+1));
        p.setTaxes(2D * (i+1));
        return p;
    }
}
