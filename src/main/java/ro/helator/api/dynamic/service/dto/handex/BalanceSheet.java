package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BalanceSheet {

    private Double cashEquivalent;
    private Double equity;
    private Double goodWill;
    private Double intangibleAssets;
    private Double tangibleAssets;
    private Double totalAssets;
    private Double totalLiabilities;

    public static List<BalanceSheet> getDefaultList() {
        List<BalanceSheet> list = new ArrayList<>();
        for (int i=0; i< 4; i++) {
            BalanceSheet b = getDefalut();
            b.setCashEquivalent(b.getCashEquivalent() * (i + 1));
            b.setEquity(b.getEquity() * (i + 1));
            b.setGoodWill(b.getGoodWill() * (i + 1));
            b.setIntangibleAssets(b.getIntangibleAssets() * (i + 1));
            b.setTangibleAssets(b.getTangibleAssets() * (i + 1));
            b.setTotalAssets(b.getTotalAssets() * (i + 1));
            b.setTotalLiabilities(b.getTotalLiabilities() * (i + 1));
            list.add(b);
        }
        return list;
    }

    private static BalanceSheet getDefalut() {
        BalanceSheet b = new BalanceSheet();
        b.setCashEquivalent(11D);
        b.setEquity(11D);
        b.setGoodWill(11D);
        b.setIntangibleAssets(11D);
        b.setTangibleAssets(11D);
        b.setTotalAssets(11D);
        b.setTotalLiabilities(11D);
        return b;
    }
}
