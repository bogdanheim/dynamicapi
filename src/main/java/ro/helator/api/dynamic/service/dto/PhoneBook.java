package ro.helator.api.dynamic.service.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PhoneBook {

    private String phoneBookName;
    private Integer id;
    private AddressBook addressBook;
}
