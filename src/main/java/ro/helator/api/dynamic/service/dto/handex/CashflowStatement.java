package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CashflowStatement {

    private Double changeCash;
    private Date dateOfFinancialStatement;
    private Double financingCashflow;
    private Double investmentCashflow;
    private Double netIncreaseInEquity;
    private Double netIncreaseInLongtermFinancialDebt;
    private Double netIncreaseInShorttermFinancialDebt;
    private Double operatingCashflow;

    public static List<CashflowStatement> getDefaultList() {
        List<CashflowStatement> list = new ArrayList<>();
        for(int i = 0; i < 3; i ++){
            CashflowStatement c = CashflowStatement.getDefault(i);
            list.add(c);
        }
        return list;
    }

    private static CashflowStatement getDefault() {
        return getDefault(0);
    }

    private static CashflowStatement getDefault(int i) {
        CashflowStatement c = new CashflowStatement();
        c.setChangeCash(10D * (i+1));
        c.setDateOfFinancialStatement(new Date());
        c.setFinancingCashflow(10D * (i+1));
        c.setInvestmentCashflow(10D * (i+1));
        c.setNetIncreaseInEquity(10D * (i+1));
        c.setNetIncreaseInLongtermFinancialDebt(10D * (i+1));
        c.setNetIncreaseInShorttermFinancialDebt(10D * (i+1));
        c.setOperatingCashflow(10D * (i+1));
        return c;
    }
}
