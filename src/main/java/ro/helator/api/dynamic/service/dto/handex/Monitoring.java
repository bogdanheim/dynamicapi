package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Monitoring {

    private List<Payment> payments;

    public static Monitoring getDefault() {
        Monitoring m = new Monitoring();
        m.setPayments(Payment.getDefaultList());
        return m;
    }
}
