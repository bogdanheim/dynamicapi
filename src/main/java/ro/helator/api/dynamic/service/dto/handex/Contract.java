package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Contract {

    private String applicationDataId;
    private Double contractValue;
    private String currency;
    private String detailsPriceAdjusmentClause;
    private List<Event> events;
    private String goodsDescription;
    private Double grossAmount;
    private String incotermsOfShipping;
    private Double interestPaymentPeriod;
    private Date invoiceDate;
    private Double shippings;
    private Double upfrontPaymentAmount;
    private Date upfrontPaymentDate;

    public static Contract getDefault(){
        return getDefault("ID1");
    }

    public static Contract getDefault(String appId) {
        Contract contract = new Contract();
        contract.setApplicationDataId(appId);
        contract.setContractValue(1_000D);
        contract.setCurrency("EUR");
        contract.setDetailsPriceAdjusmentClause("None");
        contract.setEvents(Event.getDefaultList());
        contract.setGoodsDescription("Some goods");
        contract.setGrossAmount(10D);
        contract.setIncotermsOfShipping("Term 1");
        contract.setInterestPaymentPeriod(1D);
        contract.setInvoiceDate(new Date());
        contract.setShippings(2D);
        contract.setUpfrontPaymentAmount(10D);
        contract.setUpfrontPaymentDate(new Date());
        return contract;
    }
}
