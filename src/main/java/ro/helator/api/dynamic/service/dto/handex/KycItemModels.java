package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KycItemModels {

    private String country;
    private Date dateOfBirth;
    private String houseNr;
    private String name;
    private String postalCode;
    private String street;

    public static List<KycItemModels> getDefaultList() {
        List<KycItemModels> list = new ArrayList<>();
        for(int i=0; i <3; i++){
            list.add(KycItemModels.getDefault(i));
        }
        return list;
    }

    private static KycItemModels getDefault(int i) {
        KycItemModels k = new KycItemModels();
        k.setCountry("Romania");
        k.setDateOfBirth(new Date());
        k.setHouseNr((i+1) + "");
        k.setName("House Name");
        k.setPostalCode("1234" + i);
        k.setStreet("street");
        return k;
    }


}
