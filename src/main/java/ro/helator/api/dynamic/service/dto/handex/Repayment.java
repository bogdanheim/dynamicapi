package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Repayment {

    private List<Cashflow> cashflows;
    private Integer currency;
    private Integer numberOfInstallments;
    private Integer periodInMonths;
    private Double principalAmount;

    public static Repayment getDefault() {
        Repayment r = new Repayment();
        r.setCashflows(Cashflow.getDefaultList());
        r.setCurrency(1);
        r.setNumberOfInstallments(2);
        r.setPeriodInMonths(1);
        r.setPrincipalAmount(2D);
        return r;
    }
}
