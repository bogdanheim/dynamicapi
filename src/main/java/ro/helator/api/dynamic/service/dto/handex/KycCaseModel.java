package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KycCaseModel {

    private String applicationId;
    private List<KycItemModels> varengoldKycItemModels;

    public static KycCaseModel getDefault() {
        return getDefault("ID1");
    }

    public static KycCaseModel getDefault(String applicationId) {
        KycCaseModel k = new KycCaseModel();
        k.setApplicationId(applicationId);
        k.setVarengoldKycItemModels(KycItemModels.getDefaultList());
        return k;
    }
}
