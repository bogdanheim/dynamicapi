package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Guarantee {

    private Double amountInsured;
    private Date guaranteeContractDate;
    private Date guaranteeValidUntil;
    private String guarantor;

    public static Guarantee getDefault() {
        Guarantee g = new Guarantee();
        g.setAmountInsured(11D);
        g.setGuaranteeContractDate(new Date());
        g.setGuaranteeValidUntil(new Date());
        g.setGuarantor("gg");
        return g;
    }
}
