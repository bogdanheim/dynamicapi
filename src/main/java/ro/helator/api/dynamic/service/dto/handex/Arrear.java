package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Arrear {

    private Double amount;
    private Date since;
    private String type;

    public static List<Arrear> getDefaultList() {
        List<Arrear> list = new ArrayList<>();
        for(int i=0; i<2; i++){
            list.add(Arrear.getDefault(i));
        }
        return list;
    }

    private static Arrear getDefault(int i) {
        Arrear a = new Arrear();
        a.setAmount(5D * (i+1));
        a.setSince(new Date());
        a.setType("Arrear Type");
        return a;
    }
}
