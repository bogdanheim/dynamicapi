package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class ForfaitingAgreement {

    private String currency;
    private List<Disbursement> disbursements;
    private Double purchasePrice;
    private Double securityWithhold;

    public static ForfaitingAgreement getDefault() {
        ForfaitingAgreement f = new ForfaitingAgreement();
        f.setCurrency("EUR");
        f.setDisbursements(Disbursement.getDefaultList());
        f.setPurchasePrice(15D);
        f.setSecurityWithhold(2D);
        return f;
    }
}
