package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PayoutModel {

    private String applicationId;
    private Integer payoutAmount;
    private String payoutCurrency;
    private LocalDateTime payoutDate;

    public static PayoutModel getDefault() {
        return getDefault("ID1");
    }

    public static PayoutModel getDefault(String applicationId) {
        PayoutModel p = new PayoutModel();
        p.setApplicationId(applicationId);
        p.setPayoutAmount(100);
        p.setPayoutCurrency("EUR");
        p.setPayoutDate(LocalDateTime.now());
        return p;
    }
}
