package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EulerHermesCoverage {

    private Double finalCommitmentAmountInsured;
    private Date finalCommitmentDateCoverNote;
    private Double finalCommitmentPercentageOwnRisk;
    private String finalCommitmentRetentionOfTitle;
    private Double finalCommitmentThirdPartyGuarantee;
    private Date preliminaryCommitmentDate;
    private Date preliminaryCommitmentValidUntil;

    public static EulerHermesCoverage getDefault() {
        EulerHermesCoverage e = new EulerHermesCoverage();
        e.setFinalCommitmentAmountInsured(25D);
        e.setFinalCommitmentDateCoverNote(new Date());
        e.setFinalCommitmentPercentageOwnRisk(30D);
        e.setFinalCommitmentRetentionOfTitle("Title");
        e.setFinalCommitmentThirdPartyGuarantee(20D);
        e.setPreliminaryCommitmentDate(new Date());
        e.setFinalCommitmentAmountInsured(1000D);
        e.setPreliminaryCommitmentValidUntil(new Date());
        return e;
    }
}
