package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CompanyInformation {

    private String agency;
    private String city;
    private String countryOfResidence;
    private Date dateFounded;
    private Date dateOfExternalRating;
    private Date dateRegistered;
    private String exporterId;
    private Boolean externalRating;
    private String iban;
    private String importerId;
    private String industry;
    private String lastFiscalYear;
    private String legalForm;
    private String lei;
    private String name;
    private Integer numberOfEmployees;
    private String postCode;
    private String ratingTerm;
    private String street;
    private String taxId;
    private String tradeRegisterId;
    private String tradeRegisterName;

    public static CompanyInformation getDefault() {
        CompanyInformation c = new CompanyInformation();
        c.setAgency("Agency");
        c.setCity("City");
        c.setCountryOfResidence("County of Residence");
        c.setDateFounded(new Date());
        c.setDateOfExternalRating(new Date());
        c.setDateRegistered(new Date());
        c.setExporterId("Exp1");
        c.setExternalRating(Boolean.TRUE);
        c.setIban("RX123123");
        c.setImporterId("Imp1");
        c.setIndustry("Industry");
        c.setLastFiscalYear("2018");
        c.setLegalForm("SA");
        c.setLei("2LEI");
        c.setName("My Name");
        c.setNumberOfEmployees(10);
        c.setPostCode("101010");
        c.setRatingTerm("Term");
        c.setStreet("Street");
        c.setTaxId("Tax1");
        c.setTradeRegisterId("TR1");
        c.setTradeRegisterName("TR Name");
        return c;
    }
}
