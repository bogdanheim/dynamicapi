package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class KycData {

    private String applicationId;
    private String doesPepExist;
    private String focusCountryFive;
    private String focusCountryFour;
    private String focusCountryOne;
    private String focusCountryThree;
    private String focusCountryTwo;
    private String foreignSupplier;
    private Integer totalPep;

    public static KycData getDefault() {
        KycData k = new KycData();
        k.setApplicationId("ID123");
        k.setDoesPepExist("No");
        k.setFocusCountryFive("five");
        k.setFocusCountryFour("four");
        k.setFocusCountryOne("one");
        k.setFocusCountryThree("three");
        k.setFocusCountryTwo("two");
        k.setForeignSupplier("F Supplier");
        k.setTotalPep(100);
        return k;
    }
}
