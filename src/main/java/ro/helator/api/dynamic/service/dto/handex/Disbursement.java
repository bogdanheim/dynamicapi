package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Disbursement {

    private Double amount;

    public static List<Disbursement> getDefaultList() {
        List<Disbursement> list = new ArrayList<>();
        for(int i=0; i<2; i++){
            list.add(Disbursement.getDefault(i));
        }
        return list;
    }

    private static Disbursement getDefault() {
        return getDefault(0);
    }

    private static Disbursement getDefault(int i) {
        Disbursement d = new Disbursement();
        d.setAmount(22D * (i+1));
        return d;
    }
}
