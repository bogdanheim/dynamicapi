package ro.helator.api.dynamic.service.dto.handex;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class DecisionModel {

    private Boolean accepted;
    private String applicationId;

    public static DecisionModel getDefault() {
        return getDefault("DM1");
    }

    public static DecisionModel getDefault(String applicationId) {
        DecisionModel d = new DecisionModel();
        d.setAccepted(Boolean.TRUE);
        d.setApplicationId(applicationId);
        return d;
    }
}
