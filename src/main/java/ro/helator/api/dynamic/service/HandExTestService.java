package ro.helator.api.dynamic.service;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ro.helator.api.dynamic.controller.WebidSftpNotify;
import ro.helator.api.dynamic.service.dto.handex.*;

import java.util.Arrays;
import java.util.List;

@Service
@Slf4j
public class HandExTestService {

    // APPLICATION

    public ApplicationData applicationDataCreate(ApplicationData applicationData){
        return applicationData;
    }

    public ApplicationData applicationDataFindByApplicationId(String applicationId){
        return ApplicationData.getDefault(applicationId);
    }

    public List<ApplicationData> applicationDataFindAll(){
        return Arrays.asList(ApplicationData.getDefault("ID1"),ApplicationData.getDefault("ID12"),ApplicationData.getDefault("ID123"));
    }

    // ACCOUNT

    public AccountModel accountCreate(AccountModel accountModel){
        return accountModel;
    }

    public List<AccountModel> accountFindAll(){
        return Arrays.asList(AccountModel.getDefault("ACC1"), AccountModel.getDefault("ACC2"), AccountModel.getDefault("ACC3"));
    }

    // DECISION

    public DecisionModel decisionCreate(DecisionModel decisionModel){
        return decisionModel;
    }

    public List<DecisionModel> decisionFindAll(){
        return Arrays.asList(DecisionModel.getDefault("ID1"),DecisionModel.getDefault("ID12"),DecisionModel.getDefault("ID123"));
    }

    // DOCUMENT

    public DocumentModel documentCreate(DocumentModel documentModel){
        return documentModel;
    }

    public List<DocumentModel> documentFindAll(){
        return Arrays.asList(DocumentModel.getDefault("ID1"),DocumentModel.getDefault("ID12"),DocumentModel.getDefault("ID123"));
    }

    // KYC CASE

    public KycCaseModel kycCaseCreate(KycCaseModel kycCaseModel){
        return kycCaseModel;
    }

    public List<KycCaseModel> kycCaseFindAll(){
        return Arrays.asList(KycCaseModel.getDefault("ID1"),KycCaseModel.getDefault("ID12"),KycCaseModel.getDefault("ID123"));
    }

    // MONITORING

    public Monitoring monitoringCreate(Monitoring monitoring){
        return monitoring;
    }

    public List<Monitoring> monitoringFindAll(){
        return Arrays.asList(Monitoring.getDefault());
    }

    // PAYOUT

    public PayoutModel payoutCreate(PayoutModel payoutModel){
        return payoutModel;
    }

    public List<PayoutModel> payoutFindAll(){
        return Arrays.asList(PayoutModel.getDefault("ID1"),PayoutModel.getDefault("ID12"),PayoutModel.getDefault("ID123"));
    }

    // WEBID

    public WebidModel webidCreate(WebidModel webidModel){
        return webidModel;
    }

    public List<WebidModel> webidFindAll(){
        return Arrays.asList(WebidModel.getDefault("ID1"),WebidModel.getDefault("ID12"),WebidModel.getDefault("ID123"));
    }

    // WEBID SFTP NOTIFY

    public WebidSftpNotify webidSftpNotifyCreate(WebidSftpNotify webidSftpNotify){
        return webidSftpNotify;
    }
}
