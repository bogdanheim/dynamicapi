package ro.helator.api.dynamic.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ro.helator.api.dynamic.service.HandExTestService;
import ro.helator.api.dynamic.service.dto.handex.*;

import java.util.List;

@RestController
@RequestMapping("/handex/api")
public class HandExTestController {

    private HandExTestService handExTestService;

    @Autowired
    public HandExTestController(HandExTestService handExTestService){
        this.handExTestService = handExTestService;
    }

    // APPLICATION

    @PostMapping("/applicationData/create")
    public ApplicationData applicationDataCreate(@RequestBody ApplicationData applicationData){
        return handExTestService.applicationDataCreate(applicationData);
    }

    @GetMapping("/applicationData/findByApplicationId")
    public ApplicationData applicationDataFindByApplicationId(@RequestBody String applicationId){
        return handExTestService.applicationDataFindByApplicationId(applicationId);
    }

    @GetMapping("/applicationData/findByApplicationIdNew/{applicationId}")
    public ApplicationData applicationDataFindByApplicationIdNew(@PathVariable("applicationId") String applicationId){
        return handExTestService.applicationDataFindByApplicationId(applicationId);
    }

    @GetMapping("/applicationData/findAll")
    public List<ApplicationData> applicationDataFindAll(){
        return handExTestService.applicationDataFindAll();
    }

    // ACCOUNT

    @PostMapping("/account/create")
    public AccountModel accountCreate(@RequestBody AccountModel accountModel){
        return handExTestService.accountCreate(accountModel);
    }

    @GetMapping("/account/findAll")
    public List<AccountModel> accountFindAll(){
        return handExTestService.accountFindAll();
    }

    // DECISION

    @PostMapping("/decision/create")
    public DecisionModel decisionCreate(@RequestBody DecisionModel decisionModel){
        return handExTestService.decisionCreate(decisionModel);
    }

    @GetMapping("/decision/findAll")
    public List<DecisionModel> decisionFindAll(){
        return handExTestService.decisionFindAll();
    }

    // DOCUMENT

    @PostMapping("/document/create")
    public DocumentModel documentCreate(@RequestBody DocumentModel documentModel){
        return handExTestService.documentCreate(documentModel);
    }

    @GetMapping("/document/findAll")
    public List<DocumentModel> documentFindAll(){
        return handExTestService.documentFindAll();
    }

    // KYC CASE

    @PostMapping("/kycCase/create")
    public KycCaseModel kycCaseCreate(@RequestBody KycCaseModel kycCaseModel){
        return handExTestService.kycCaseCreate(kycCaseModel);
    }

    @GetMapping("/kycCase/findAll")
    public List<KycCaseModel> kycCaseFindAll(){
        return handExTestService.kycCaseFindAll();
    }

    // MONITORING

    @PostMapping("/monitoring/create")
    public Monitoring monitoringCreate(@RequestBody Monitoring monitoring){
        return handExTestService.monitoringCreate(monitoring);
    }

    @GetMapping("/monitoring/findAll")
    public List<Monitoring> monitoringFindAll(){
        return handExTestService.monitoringFindAll();
    }

    // PAYOUT

    @PostMapping("/payout/create")
    public PayoutModel payoutCreate(@RequestBody PayoutModel payoutModel){
        return handExTestService.payoutCreate(payoutModel);
    }

    @GetMapping("/payout/findAll")
    public List<PayoutModel> payoutFindAll(){
        return handExTestService.payoutFindAll();
    }

    // WEBID

    @PostMapping("/webid/create")
    public WebidModel webidCreate(@RequestBody WebidModel webidModel){
        return handExTestService.webidCreate(webidModel);
    }

    @GetMapping("/webid/findAll")
    public List<WebidModel> webidFindAll(){
        return handExTestService.webidFindAll();
    }

    // WEBID SFTP NOTIFY

    @PostMapping("/webid/sftp/notify/create")
    public WebidSftpNotify webidSftpNotifyCreate(@RequestBody WebidSftpNotify webidSftpNotify){
        return handExTestService.webidSftpNotifyCreate(webidSftpNotify);
    }
}
