package ro.helator.api.dynamic.controller;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class WebidSftpNotify {

    private String applicationId;
    private Boolean filesAvailableOnSftpServer;
}
