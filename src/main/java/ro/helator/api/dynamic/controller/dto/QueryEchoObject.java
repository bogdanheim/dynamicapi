package ro.helator.api.dynamic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class QueryEchoObject {

    private String queryString;
    private Integer queryInteger;
    private BigDecimal queryDecimal;
    private Boolean queryBoolean;
}
