package ro.helator.api.dynamic.controller.dto;

import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
@Builder
public class EchoObj {
    private String myString;
    private Integer mtInt;
    private Boolean myBoolean;
}
