package ro.helator.api.dynamic.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import ro.helator.api.dynamic.controller.dto.onboarding.CheckResult;
import ro.helator.api.dynamic.controller.dto.onboarding.CustomerDetails;

@RestController
@RequestMapping("/onboarding")
public class OnBoardingController {

    @PostMapping("/kyc")
    public CheckResult getKyc(@RequestBody CustomerDetails request) {
        return new CheckResult(Math.random() <= 0.8 ? Boolean.TRUE : Boolean.FALSE);
    }


    @PostMapping("/aml")
    public CheckResult amlCheck(@RequestBody CustomerDetails request) {
        return new CheckResult(Math.random() <= 0.8 ? Boolean.TRUE : Boolean.FALSE);
    }
}
