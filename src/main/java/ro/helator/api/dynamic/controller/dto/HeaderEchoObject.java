package ro.helator.api.dynamic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class HeaderEchoObject {

    private String headerString;
    private Integer headerInteger;
    private BigDecimal headerDecimal;
    private Boolean headerBoolean;
}
