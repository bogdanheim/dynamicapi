package ro.helator.api.dynamic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EchoObject {

    private PathEchoObject path;
    private QueryEchoObject query;
    private HeaderEchoObject header;
    private BodyEchoObject bodyEchoObject;

}
