package ro.helator.api.dynamic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BodyEchoObject {

    private String bodyString;
    private Integer bodyInteger;
    private BigDecimal bodyDecimal;
    private Boolean bodyBoolean;
    private BodyEchoSubObject bodyObject;
    private List<BodyEchoSubObject> bodyObjectList;
}
