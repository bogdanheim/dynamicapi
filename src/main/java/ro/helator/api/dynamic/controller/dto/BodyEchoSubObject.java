package ro.helator.api.dynamic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class BodyEchoSubObject {

    private String bodyString;
    private Integer bodyInteger;
    private BigDecimal bodyDecimal;
    private Boolean bodyBoolean;
}
