package ro.helator.api.dynamic.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class EchoObjectP {

    private PathEchoObject path;
    private QueryEchoObject query;
    private HeaderEchoObject header;
    private String bodyString;
}
