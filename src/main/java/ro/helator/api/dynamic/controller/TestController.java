package ro.helator.api.dynamic.controller;

import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ResponseHeader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ro.helator.api.dynamic.controller.dto.*;
import ro.helator.api.dynamic.service.TestAPIService;
import ro.helator.api.dynamic.service.dto.AddressBook;
import ro.helator.api.dynamic.service.dto.Contact;
import ro.helator.api.dynamic.service.dto.PhoneBook;
import ro.helator.api.dynamic.service.dto.PhoneBookRequest;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/test/api")
public class TestController {

    private TestAPIService testAPIService;

    @Autowired
    public TestController(TestAPIService testAPIService) {
        this.testAPIService = testAPIService;
    }

    @PostMapping("/getPhoneBook")
    public PhoneBook getPhoneBook(@RequestBody PhoneBookRequest request) {
        PhoneBook book = new PhoneBook();
        book.setPhoneBookName("Test Book Name");
        book.setId(request.getId());
        List<Contact> contacts = new ArrayList<>();
        contacts.add(new Contact("Contact 1","+40700100200"));
        contacts.add(new Contact("Contact 2","+40700111222"));
        AddressBook addressBook = new AddressBook(contacts);
        book.setAddressBook(addressBook);
        return book;
    }

    @PostMapping("/getBack")
    public ResponseEntity<EchoObj> getBack(@RequestBody EchoObj echo){
        if(echo.getMtInt() != null) {
            switch (echo.getMtInt()) {
                case 401:
                    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
                case 403:
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                case 404:
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                default:
                    return new ResponseEntity<>(echo, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(echo, HttpStatus.OK);
    }

    @PostMapping("/getBack2")
    public ResponseEntity<EchoObj> getBack2(@RequestBody EchoObj echo){
        if(echo.getMtInt() != null) {
            switch (echo.getMtInt()) {
                case 401:
                    return new ResponseEntity<>(HttpStatus.UNAUTHORIZED);
                case 403:
                    return new ResponseEntity<>(HttpStatus.FORBIDDEN);
                case 404:
                    return new ResponseEntity<>(HttpStatus.NOT_FOUND);
                default:
                    return new ResponseEntity<>(echo, HttpStatus.OK);
            }
        }
        return new ResponseEntity<>(echo, HttpStatus.OK);
    }

    @PostMapping("/echo/{value}/getBack")
//    @ApiImplicitParams({
//            @ApiImplicitParam(name = "headerString", dataType = "String", paramType = "header"),
//            @ApiImplicitParam(name = "headerInteger", dataType = "Integer", paramType = "header"),
//            @ApiImplicitParam(name = "headerDecimal", dataType = "BigDecimal", paramType = "header"),
//            @ApiImplicitParam(name = "headerBoolean", dataType = "Boolean", paramType = "header"),
//            @ApiImplicitParam(name = "X-head-boolean", dataType = "Boolean", paramType = "header"),
//    })
    public EchoObject echoBack(@RequestHeader("X-head-string") String headerString, @RequestHeader("X-head-integer") Integer headerInteger,
                               @RequestHeader("X-head-decimal") BigDecimal headerDecimal, @RequestHeader("X-head-boolean") Boolean headerBoolean,
                               @RequestParam("queryString") String quesryString, @RequestParam("queryInteger") Integer queryInteger,
                               @RequestParam("queryDecimal") BigDecimal queryDecimal, @RequestParam("queryBoolean") Boolean queryBoolean,
                               @PathVariable("value") String value, @RequestBody BodyEchoObject bodyObject) {

        EchoObject echo = new EchoObject();
        echo.setHeader(new HeaderEchoObject(headerString, headerInteger, headerDecimal, headerBoolean));
        echo.setPath(new PathEchoObject(value));
        echo.setQuery(new QueryEchoObject(quesryString, queryInteger, queryDecimal, queryBoolean));
        echo.setBodyEchoObject(bodyObject);
        return echo;
    }

    @PostMapping("/echo/{value}/getBackPrim")
    public EchoObjectP echoBackPrim(@RequestHeader("X-head-string") String headerString, @RequestHeader("X-head-integer") Integer headerInteger,
                               @RequestHeader("X-head-decimal") BigDecimal headerDecimal, @RequestHeader("X-head-boolean") Boolean headerBoolean,
                               @RequestParam("queryString") String quesryString, @RequestParam("queryInteger") Integer queryInteger,
                               @RequestParam("queryDecimal") BigDecimal queryDecimal, @RequestParam("queryBoolean") Boolean queryBoolean,
                               @PathVariable("value") String value, @RequestBody String bodyString) {

        EchoObjectP echo = new EchoObjectP();
        echo.setHeader(new HeaderEchoObject(headerString, headerInteger, headerDecimal, headerBoolean));
        echo.setPath(new PathEchoObject(value));
        echo.setQuery(new QueryEchoObject(quesryString, queryInteger, queryDecimal, queryBoolean));
        echo.setBodyString(bodyString);
        return echo;
    }
}
