package ro.helator.api.dynamic.controller.dto.onboarding;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CustomerDetails {

    private String idNumber;
    private String idType;
    private String firstName;
    private String lastName;
    private Date dateOfBirth;
}
