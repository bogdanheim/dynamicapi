package ro.helator.api.dynamic.controller.dto.onboarding;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CheckResult {

    private Boolean check;
    private String result;

    public CheckResult(Boolean check) {
        this.check = check;
        this.result = this.check ? "Compliant" : "High Risk";
    }
}
